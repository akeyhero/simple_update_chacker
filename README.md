# The Simple Update Checker

The Simple Update Checker will detect updates on a web page and notify you via emails.

## Setup

```bash
$ bundle install
```

The Simple Update Checker also requires `phantomjs` ready to run.

## Run

```bash
$ ./run.rb ${EMAIL} ${URL} ${CSS SELECTOR} ${INTERVAL IN MINS}
```

You may be happy with `tmux` or `screen` .
